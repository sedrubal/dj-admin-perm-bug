# Reproduce Bug (?)

```sh
poetry install
cd admin_has_perm_reproduction
./manage.py migrate
./manage.py loaddata users secrets
./manage.py runserver
```

Login with

| E-Mail | Password | Permissions |
|---|---|---|
| admin@localhost.localdomain | admin | app1, app2 |
| staff@localhost.localdomain | staff | app1 |

With the staff login go to <http://127.0.0.1:8000/admin/app2/secrettestmodel/1/change/>
