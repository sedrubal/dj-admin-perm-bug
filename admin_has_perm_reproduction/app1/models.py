from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _


class UserManager(BaseUserManager):
    def create_user(self, email, name, password=None):
        if not email:
            raise ValueError("Users must have an email address")

        user = self.model(
            email=self.normalize_email(email),
            name=name,
        )

        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, email, name, password=None):
        user = self.create_user(
            email,
            password=password,
            name=name,
        )
        user.is_superuser = True
        user.is_staff = True
        user.save(using=self._db)

        return user


class User(AbstractBaseUser):
    """A user."""

    USERNAME_FIELD = "email"
    EMAIL_FIELD = "email"
    REQUIRED_FIELDS = ("name",)

    objects = UserManager()

    email = models.EmailField(
        verbose_name=_("Email Address"),
        unique=True,
        max_length=255,
        help_text=_("The email address of this user. Also used for login."),
    )
    name = models.CharField(
        verbose_name=_("Name"),
        max_length=255,
        help_text=_("The Name of the user."),
    )
    is_active = models.BooleanField(
        verbose_name=_("Is Active"),
        default=True,
        help_text=_("Is this an active user?"),
    )
    is_staff = models.BooleanField(
        verbose_name=_("Is Staff"),
        default=False,
        help_text=_("Does this user belong to the staff team?"),
    )
    is_superuser = models.BooleanField(
        _("Superuser Status"),
        default=False,
        help_text=_(
            "Designates that this user has all permissions without "
            "explicitly assigning them."
        ),
    )
    date_joined = models.DateTimeField(
        verbose_name=_("Date Joined"),
        default=timezone.now,
        help_text=_("The date of creation of the user."),
    )
    last_login = models.DateTimeField(
        verbose_name=_("Last Login"),
        blank=True,
        null=True,
        help_text=_("The date of the last login of the user."),
    )

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    def get_full_name(self) -> str:
        return self.name

    def get_short_name(self) -> str:
        return self.name

    def has_perm(self, perm: str, obj=None):
        "Does the user have a specific permission?"

        return True

    def has_module_perms(self, app_label: str):
        "Does the user have permissions to view the app `app_label`?"

        return self.is_superuser or app_label == "app1"
