from django.contrib import admin
from django.contrib.auth.admin import Group

from .models import User

# Register your models here.

admin.site.unregister(Group)


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "email",
    )
    readonly_fields = ("date_joined", "last_login")
    fieldsets = [
        (
            None,
            {
                "fields": (
                    "email",
                    "name",
                    "is_active",
                    "date_joined",
                    "last_login",
                    "is_staff",
                    "is_superuser",
                )
            },
        )
    ]
