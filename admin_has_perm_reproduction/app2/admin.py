from django.contrib import admin

from .models import SecretTestModel
# Register your models here.


@admin.register(SecretTestModel)
class UserAdmin(admin.ModelAdmin):
    list_display = (
        "totally_secret_field",
    )
    fieldsets = [
        (
            None,
            {
                "fields": (
                    "totally_secret_field",
                )
            },
        )
    ]
