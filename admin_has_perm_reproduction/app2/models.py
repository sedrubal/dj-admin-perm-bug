from django.db import models

class SecretTestModel(models.Model):
    totally_secret_field = models.CharField(
        max_length=255,
    )
